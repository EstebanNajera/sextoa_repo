class AddZombieToBrain < ActiveRecord::Migration[5.0]
  def change
    add_reference :brains, :Zmobie, foreign_key: true
  end
end
